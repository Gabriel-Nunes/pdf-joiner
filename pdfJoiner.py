from tkinter import messagebox, Tk, filedialog
from PyPDF2 import PdfFileMerger, PdfFileReader
from subprocess import Popen
import os


def alert(window_title, message):
    root = Tk()
    root.withdraw()
    messagebox.showinfo(window_title, message)


def select_files():
    root = Tk()
    root.withdraw()
    root.filenames = filedialog.askopenfilenames(initialdir="/", title="Selecione os arquivos PDF...",
                                                filetypes=(("pdf files", "*.pdf"), ("all files", "*.*")))
    return list(root.filenames)


# introducing message
alert("Aviso", "Os arquivos serão unidos em ordem alfabética,"
                     "de acordo com os nomes dos originais.")

# select files to join
originalFiles = select_files()
originalFiles.sort()
folder = os.path.dirname(originalFiles[0])

pdfMerger = PdfFileMerger()  # initialize a merger object to do the trick

# open the pdf documents
for filename in originalFiles:
    pdfMerger.append(PdfFileReader(filename))

newFile = open(folder + r'\GERAL.pdf', 'wb')
pdfMerger.write(newFile)
newFile.close()

# open the final pdf folder
Popen(r'explorer /select,' + os.path.abspath(originalFiles[0]))
