# PDF Joiner

This program joins the content of different PDF files in a single one.

The final content will be sorted alphabetically according to the sources names. 

Usage:

- run 'pdfJoiner.exe'
- select original PDFs
- check "GERAL.pdf" at the same folder of sources files.

